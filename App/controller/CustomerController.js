// import express
const express = require('express');
// import model 
const CustomerModel = require('../model/CustomerModel')
// import mongoose
const mongoose = require('mongoose');
const OrderModel = require('../model/OrderModel');
// tạo post 
const createCustomer = (request, response) => {
    // thu thập dữ liệu
    let body = request.body;
    // validate
    if (!body.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if (!body.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let customerModelData = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address
    }
    CustomerModel.create(customerModelData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create customer success",
                data: data
            })
        }
    })
}
// tạo get all  
const getAllCustomer = (request, response) => {
    CustomerModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all customer successfully",
                data: data
            })
        }
    })
}
// tạo get by id 
const getCustomerById = (request, response) => {
    // lấy param 
    let customerId = request.params.customerId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "customer Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    CustomerModel.findById(customerId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get customer by id success" + customerId,
                data: data
            })
        }
    })
}
const getCustomerByEmail = (req, res) => {
    let customerEmail = { email: req.params.customerEmail }
    console.log(customerEmail)
    CustomerModel.findOne(customerEmail, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get customer by email success" + customerEmail,
                data: data
            })
        }
    })
}
const UpdateCustomerByEmail = (req, res) => {
    let customerEmail = { email: req.params.customerEmail }
    let newCustomer = req.body
    console.log(customerEmail)
    //kiểm tra sdt đã tồn tại chưa
    CustomerModel.findOne({ phone: newCustomer.phone }, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            //nếu sdt đã tồn tại và là của 1 người nào đó
            if (data && data.email != req.params.customerEmail) {
                console.log("aaaa")
                return res.status(500).json({
                    status: "Error 500: Internal sever Error",
                    message: "số điện thoại đã được đăng ký"
                })
            }
            else {
                CustomerModel.findOne(customerEmail, (error, data) => {
                    if (error) {
                        return res.status(500).json({
                            status: "Error 500: Internal sever Error",
                            message: error.message
                        })
                    } else {
                        //nếu customer đã tồn tại thì update customer
                        if (data) {
                            CustomerModel.findByIdAndUpdate(data._id, {...newCustomer}, (error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                        status: "Error 500: Internal sever error",
                                        message: error.message
                                    })
                                } else {
                                    return res.status(200).json({
                                        status: "Success: Update customer success",
                                        data: data
                                    })
                                }
                            })
                        }
                        //nếu customer chuaw tồn tại thì tạo mới customer
                        else {
                            let customerModelData = {
                                _id: mongoose.Types.ObjectId(),
                                ...newCustomer
                            }
                            CustomerModel.create(customerModelData, (error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                        status: "Error 500: Internal sever error",
                                        message: error.message
                                    })
                                } else {
                                    return res.status(200).json({
                                        status: "Success: Update customer success",
                                        data: data
                                    })
                                }
                            })
                        }
            
                    }
                })
            }
        }
    })
    
}
// tạo post 
const updateCustomer = (request, response) => {

    // thu thập dữ liệu
    let customerId = request.params.customerId;
    let body = request.body;
    // validate
    // validate
    if (!body.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "fullName is required"
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if (!body.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let customerUpdate = {
        fullName: body.fullName,
        address: body.address,
        phone: body.phone,
        email: body.email,
        timeUpdated: new Date()
    }
    CustomerModel.findByIdAndUpdate(customerId, customerUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update product type success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteCustomer = (request, response) => {
    // B1: thu thập dữ liệu
    let customerId = request.params.customerId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "customer Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    CustomerModel.findById(customerId,(error,data)=>{
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            let orders = data.orders
            OrderModel.deleteMany({_id: { $in: orders }},(err,res)=>{
                if (err){
                    return response.status(500).json({
                        status: "Error 500: Internal sever error",
                        message: error.message
                })}
                else {
                    CustomerModel.findByIdAndDelete(customerId, (error, data) => {
                        if (error) {
                            return response.status(500).json({
                                status: "Error 500: Internal sever error",
                                message: error.message
                            })
                        } else {
                            return response.status(200).json({
                                status: "Success: Update customer success" + customerId + " Success ",
                            })
                        }
                    })
                }
            })
        }
    })
   
}

module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer,
    getCustomerByEmail,
    UpdateCustomerByEmail
}