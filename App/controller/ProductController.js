// import express
const express = require('express');
// import model 
const productModel = require('../model/ProductModel')
// import mongoose
const mongoose = require('mongoose');
// tạo post 
const createProduct = (request, response) => {
    // thu thập dữ liệu
    let body = request.body;
    // validate
    if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if (!body.type) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "type is required"
        })
    }
    if (!body.imageUrl) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "imageUrl is required"
        })
    }
    if (!body.buyPrice) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "buyPrice is required"
        })
    }
    if (!body.promotionPrice) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "promotionPrice is required"
        })
    }
    if (!body.amount) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "amount is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let productModelData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }
    productModel.create(productModelData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create prodduct success",
                data: data
            })
        }
    })
}
// tạo get all  
const getAllProduct = (request, response) => {
    //const typeArray = request.query.type.split(",")
    const filter1 = {}
    if (request.query.name) {filter1.name = { $regex: `.*${request.query.name}.*`, $options: 'i' }}
    if (request.query.min ) {filter1.promotionPrice = { $gte: `${parseInt(request.query.min)}` }}
    if (request.query.max ) {filter1.promotionPrice = { $lte: `${parseInt(request.query.max)}` }}
    if (request.query.min && request.query.max ) {filter1.promotionPrice = { $gte: `${parseInt(request.query.min)}`, $lte: `${parseInt(request.query.max)}`}}
    if (request.query.type) {filter1.type = { $in: request.query.type.split(",") }}
    
    
    productModel.find(filter1,(error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all product successfully",
                data: data
            })
        }
    }).limit((request.query.limit)?request.query.limit:0).skip((request.query.skip)?request.query.skip:0)
}
// tạo get by id 
const getProductById = (request, response) => {
    // lấy param 
    let productId = request.params.productId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "product Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    productModel.findById(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get product by id success" + productId,
                data: data
            })
        }
    })
}
// tạo post 
const updateProduct = (request, response) => {

    let productId = request.params.productId;
    let productUpdate = {...request.body,timeUpdated: Date.now()};
/*     if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if (!body.type) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "type is required"
        })
    }
    if (!body.buyPrice) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "buyPrice is required"
        })
    }
    if (!body.promotionPrice) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "promotionPrice is required"
        })
    }
    if (!body.amount) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "amount is required"
        })
    } */
    // b3 sử dụng cơ sở dữ liệu
   
    productModel.findByIdAndUpdate(
        productId, 
        productUpdate, 
        (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update product type success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteProduct = (request, response) => {
    // B1: thu thập dữ liệu
    let productId = request.params.productId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "productType Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update product success" + productId + " Success ",
            })
        }
    })
}

module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}