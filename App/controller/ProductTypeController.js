// import express
const express = require('express');
// import model 
const productType = require('../model/ProductType')
// import mongoose
const mongoose = require('mongoose');
// tạo post 
const CreateProductType = (request, response) => {
    // thu thập dữ liệu
    let body = request.body;
    // validate
    if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Lastname is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let productTypeData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description : body.description,
    }
    productType.create(productTypeData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create prize success",
                data: data
            })
        }
    })
}
// tạo get all  
const GetAllProductType = (request, response) => {
    productType.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all product type successfully",
                data: data
            })
        }
    })
}
// tạo get by id 
const GetProductTypeByID = (request, response) => {
    //B1: thu thập dữ liệu
    let productTypeId = request.params.productTypeId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "productType Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    productType.findById(productTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get product by id success" + productTypeId,
                data: data
            })
        }
    })

}
// tạo post 
const UpdateProductType = (request, response) => {
    // thu thập dữ liệu
    let productTypeId = request.params.productTypeId;
    let body = request.body;
    // validate
    if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "productType Id is not valid"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let productTypeUpdate = {
        name: body.name,
        description : body.description,
        timeUpdated: new Date()
    }
    productType.findByIdAndUpdate(productTypeId, productTypeUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update product type success",
                data: data
            })
        }
    })
}
// tạo post 
const DeleteProductType = (request, response) => {
    // B1: thu thập dữ liệu
    let productTypeId = request.params.productTypeId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "productType Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    productType.findByIdAndDelete(productTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update product type success" + productTypeId + " Success ",
            })
        }
    })
}

module.exports = {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType
}