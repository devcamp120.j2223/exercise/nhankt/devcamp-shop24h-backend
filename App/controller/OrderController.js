// import express
const express = require('express');
// import model 
const OrderModel = require('../model/OrderModel')
const CustomerModel = require('../model/CustomerModel')
// import mongoose
const mongoose = require('mongoose');
const { create } = require('../model/OrderModel');
const { request, response } = require('express');
// tạo post 
const createOrderOfCustomer = (request, response) => {
    // thu thập dữ liệu
    let body = request.body;
    let customerId = request.params.customerId;
    // validate
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "customer ID is invalid"
        })
    }
    if (!body.shippedDate) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "shippedDate is required"
        })
    }
    if (!body.orderDetail) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetail is required"
        })
    }
    if (!body.cost) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "cost is required"
        })
    }
    if (!body.shippingAddress) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "cost is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let orderModelData = {
        _id: mongoose.Types.ObjectId(),
        shippedDate: body.shippedDate,
        orderDetail: body.orderDetail,
        cost: body.cost,
        shippingAddress: body.shippingAddress
    }
    OrderModel.create(orderModelData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            CustomerModel.findByIdAndUpdate(customerId, {
                $push: { orders: data._id }
            },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}
// get all orders of customer
const getAllOrderOfCustomer = (request, response) => {
    // b1 lấy dữ liệu
    let customerId = request.params.customerId;
    // validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "order Id is not valid"
        })
    }
    // b3 sử dụng cơ sơ dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    CustomerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get all order of customer success",
                    data: data.orders
                })
            }
        })
}
// tạo get all  
const getAllOrder = (request, response) => {

    if (Object.keys(request.query).length == 0) {
        OrderModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal sever Error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get all orders successfully",
                    data: data
                })
            }
        })
    }
    else {
        let OrdersId = request.query.id.split(",")
        OrderModel.find({ "_id": { "$in": OrdersId } }, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal sever Error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get all orders successfully",
                    data: data
                })
            }
        })
    }
}
// tạo get by id 
const getOrderById = (request, response) => {
    // lấy param 
    let ordersId = request.params.ordersId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(ordersId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "order Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    OrderModel.findById(ordersId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get orders by id success" + ordersId,
                data: data
            })
        }
    })
}

// tạo post 
const updateOrder = (request, response) => {

    // thu thập dữ liệu
    let ordersId = request.params.ordersId;
    let body = request.body;
    // validate
    if (!body.shippedDate) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "shippedDate is required"
        })
    }
    if (!body.orderDetail) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetail is required"
        })
    }
    if (!body.cost) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "cost is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    /*  let orderupdate = {
         shippedDate: body.shippedDate,
         orderDetail: body.orderDetail,
         cost: body.cost,
     } */
    let orderupdate = { ...body, timeUpdated: new Date() }
    OrderModel.findByIdAndUpdate(ordersId, orderupdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update orders type success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteOrder = (request, response) => {
    // B1: thu thập dữ liệu
    let ordersId = request.params.ordersId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(ordersId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orders Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    OrderModel.findByIdAndDelete(ordersId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update orders success" + ordersId + " Success ",
            })
        }
    })
}
// tạo order 

const createOrder1 = (request, response) => {
    let customer = {
        _id: mongoose.Types.ObjectId(),
        fullName: request.body.customer.fullName,
        phone: request.body.customer.phone,
        email: request.body.customer.email,
    }
    let order = request.body.order
    OrderModel.create({
        _id: mongoose.Types.ObjectId(),
        shippedDate: order.shippedDate,
        orderDetail: order.orderDetail,
        cost: order.cost,
        shippingAddress: order.shippingAddress
    }, (error, orderData) => {
        if (error) {//tạo order bị lỗi
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message

            })
        } else {//tạo thành công order
            //tìm xem custom đã tồn tại chưa = sdt
            console.log("đã tạo được order")
            CustomerModel.findOne({
                phone: customer.phone
            }, (errorFindUser, userExist) => {
                //Lỗi tìm
                if (errorFindUser) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: errorFindUser.message
                    })
                } else {
                    if (!userExist) {
                        // Không tồn tại custom có sdt đó =>tạo user mới
                        console.log("đã tìm custom và ko thấy")
                        CustomerModel.create(customer, (error, newUser) => {
                            if (error) {
                                console.log("ko tạo được")
                                return response.status(500).json({
                                    status: "Error 500: Internal sever Error",
                                    message: error.message
                                })
                            } else {
                                CustomerModel.findByIdAndUpdate(newUser._id,
                                    { $push: { orders: orderData._id } }, (error, userData) => {
                                        response.status(200).json({
                                            status: "Success: create orders success",
                                            data: {
                                                _id: newUser._id,
                                                shippedDate: orderData.shippedDate,
                                                orderDetail: orderData.orderDetail,
                                                cost: orderData.cost,
                                                shippingAddress: orderData.shippingAddress,
                                                fullName: newUser.fullName,
                                                email: newUser.email,
                                                phone: newUser.phone,
                                            }
                                        })
                                    })
                            }
                        })
                    }
                    else {
                        //có tồn tại (userExist==true) custom với sdt nên chỉ cần update
                        CustomerModel.findByIdAndUpdate(userExist._id,
                            { $push: { orders: orderData._id } }, (error, userData) => {
                                response.status(200).json({
                                    status: "Success: create orders success",
                                    data: {
                                        _id: userExist._id,
                                        shippedDate: orderData.shippedDate,
                                        orderDetail: orderData.orderDetail,
                                        cost: orderData.cost,
                                        shippingAddress: orderData.shippingAddress,
                                        fullName: userExist.fullName,
                                        email: userExist.email,
                                        phone: userExist.phone,
                                    }
                                })
                            })
                    }



                }
            })
        }
    })
}
const createOrder = (request, response) => {
    let customer = {
        //_id: mongoose.Types.ObjectId(),
        fullName: request.body.customer.fullName,
        phone: request.body.customer.phone,
        email: request.body.customer.email,
    }

    let order = request.body.order

    CustomerModel.findOne({ phone: customer.phone }, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            //nếu sdt đã tồn tại và là của 1 người khác 
            if (data && data.email != customer.email) {
                console.log("aaaa")
                return response.status(500).json({
                    status: "Error 500: Internal sever Error",
                    message: "số điện thoại đã được đăng ký"
                })
            }
            //nếu sdt chưa tồn tại hoặc là số đt đã đk của khách
            else {
                CustomerModel.findOne({email:customer.email}, (error, data) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever Error",
                            message: error.message
                        })
                    } else {
                        //đã tồn tại khách hàng cập nhật đơn hàng và thông tin cho khách
                        if (data) {
                            //thêm đơn hàng 
                            OrderModel.create({
                                _id: mongoose.Types.ObjectId(),
                                customerId: data._id,
                                shippedDate: order.shippedDate,
                                orderDetail: order.orderDetail,
                                cost: order.cost,
                                shippingAddress: order.shippingAddress
                            }, (error, orderData) => {
                                if (error) {//tạo order bị lỗi
                                    response.status(500).json({
                                        status: "Error 500: Internal sever Error",
                                        message: error.message

                                    })
                                }
                                else {
                                    CustomerModel.findByIdAndUpdate(data._id,
                                        { $push: { orders: orderData._id },$set :{...customer} }, (error, userData) => {
                                            response.status(200).json({
                                                status: "Success: create orders success",
                                                data: {
                                                    _id: data._id,
                                                    shippedDate: orderData.shippedDate,
                                                    orderDetail: orderData.orderDetail,
                                                    cost: orderData.cost,
                                                    shippingAddress: orderData.shippingAddress,
                                                    fullName: data.fullName,
                                                    email: data.email,
                                                    phone: data.phone,
                                                }
                                            })
                                        })
                                }
                            })
                        }
                        //nếu customer chuaw tồn tại thì tạo mới customer rồi thêm đơn hàng
                        else {
                            //thêm đơn hàng 
                            let customerModelData = {
                                _id: mongoose.Types.ObjectId(),
                                ...customer,
                            }
                            OrderModel.create({
                                _id: mongoose.Types.ObjectId(),
                                customerId: customerModelData._id,
                                shippedDate: order.shippedDate,
                                orderDetail: order.orderDetail,
                                cost: order.cost,
                                shippingAddress: order.shippingAddress
                            }, (error, orderData) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal sever error",
                                        message: error.message
                                    })
                                } else {
                                    CustomerModel.create({...customerModelData,orders:[orderData._id]}, (error, data) => {
                                        if (error) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal sever error",
                                                message: error.message
                                            })
                                        } else {


                                            return response.status(200).json({
                                                status: "Success: Update customer success",
                                                data: data
                                            })
                                        }
                                    })
                                }
                            })

                        }

                    }
                })
            }
        }
    })

}
module.exports = {
    createOrder,
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder
}