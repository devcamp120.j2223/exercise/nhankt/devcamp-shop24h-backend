// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// khỏi tạo shema
const Schema = mongoose.Schema;

const OrderModel = new Schema({
    _id: mongoose.Types.ObjectId,
    shippedDate: {
        type: Date,
        required: true,
    },
    note: {
        type: String
    },
    orderDetail: {
        type: Array,
        required: true,
    },
    shippingAddress:{
        city:{type: String,required: true},
        district:{type: String,required: true},
        ward:{type: String,required: true},
        street:{type: String,required: true}
    },
    orderStatus:{
        type: String,
        required: true,
        default: "open"
    },
    customerId:{
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "customer"
    },

    cost: {
        type: Number,
        default: 0,
        required: true,
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("orders", OrderModel)