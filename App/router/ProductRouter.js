// import express
const express = require('express');
// import controller
const { createProduct, getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct } = require("../controller/ProductController");
// import middleware
const middlewareApp = require("../../App/middlewares/middleware");
// gán router
const router = express.Router();
// sử dụng middleware
router.use(middlewareApp);
// CreateProductType
router.post("/product", createProduct);
// CreateProductType
router.get("/product", getAllProduct);
// CreateProductType
router.get("/product/:productId", getProductById);
// CreateProductType
router.put("/product/:productId", updateProduct);
// CreateProductType
router.delete("/product/:productId", deleteProduct);
// export router
module.exports = router;