// import express
const express = require('express');
// import controller
const { createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer,
    getCustomerByEmail,
    UpdateCustomerByEmail } = require("../controller/CustomerController");
// import middleware
const middlewareApp = require("../../App/middlewares/middleware");
// gán router
const router = express.Router();
// sử dụng middleware
router.use(middlewareApp);
// CreateProductType
router.post("/customer", createCustomer);
// CreateProductType
router.get("/customer", getAllCustomer);
// CreateProductType
router.get("/customer/:customerId", getCustomerById);
//getCustomer by email
router.get("/customer/email/:customerEmail", getCustomerByEmail);
// CreateProductType
router.put("/customer/:customerId", updateCustomer);
// CreateProductType
router.put("/customer/email/:customerEmail", UpdateCustomerByEmail);
// CreateProductType
router.delete("/customer/:customerId", deleteCustomer);
// export router
module.exports = router;