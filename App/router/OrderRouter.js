// import express
const express = require('express');
// import controller
const { 
    createOrder,
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder } = require("../controller/OrderController");
// import middleware
const middlewareApp = require("../../App/middlewares/middleware");
// gán router
const router = express.Router();
// sử dụng middleware
router.use(middlewareApp);
// Create order of customer
router.post("/customer/:customerId/orders", createOrderOfCustomer);
// Create order
router.post("/orders", createOrder);
// get all orders of customer
router.get("/customer/:customerId/orders", getAllOrderOfCustomer);
// get all order
router.get("/orders", getAllOrder);
// get order by id
router.get("/orders/:ordersId", getOrderById);
// update order
router.put("/orders/:ordersId", updateOrder);
// delete order
router.delete("/orders/:ordersId", deleteOrder);
// export router
module.exports = router;