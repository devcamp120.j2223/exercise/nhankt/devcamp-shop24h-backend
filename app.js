// import express from 'express';
const express = require('express');
// import mongoose
const mongoose = require('mongoose');
// import router
const ProductTypeRouter = require('./App/router/ProductTypeRouter');
const ProductRouter = require('./App/router/ProductRouter');
const CustomerRouter = require('./App/router/CustomerRouter');
const OderRouter = require('./App/router/OrderRouter');
// chạy app
const app = express();
// khai báo cổng
const port = 8000;
// khỏi tạo json
app.use(express.json());
// encode
app.use(express.urlencoded({
    extended: true,
}))
// sử dụng mongoose
mongoose.connect("mongodb://localhost:27017/CRUD_SHOP24", (error) => {
    if (error) {
        throw error
    }
    console.log("mongodb connected successfully!")
})
// sử dụng router
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use("/", ProductTypeRouter);
app.use("/", ProductRouter);
app.use("/", CustomerRouter);
app.use("/", OderRouter);
// console port 
app.listen(port, () => {
    console.log('listening on port ' + port);
})
